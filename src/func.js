const getSum = (strg1, strg2) => {
  if (typeof strg1 !== 'string' || typeof strg2 !== 'string') {
    return false;
  }
  if (/[a-zA-Z]/.test(strg1) || /[a-zA-Z]/.test(strg2)) {
    return false;
  }
  let str1 = strg1.split('').map(Number).reverse();
  let str2 = strg2.split('').map(Number).reverse();
  if (strg1.length < strg2.length) {
    const temp = str2;
    str2 = str1;
    str1 = temp;
  }
  let remain = 0;
  for (let i = 0; i < str1.length; i += 1) {
    str1[i] += (str2[i] || 0) + remain;
    if (str1[i] > 9 && i !== str1.length - 1) {
      str1[i] %= 10;
      remain = 1;
    } else {
      remain = 0;
    }
  }
  return str1.reverse().join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCnt = 0;
  let commentsCnt = 0;
  for (let i = 0; i < listOfPosts.length; i += 1) {
    listOfPosts[i].author == authorName ? postsCnt += 1 : postsCnt += 0;
    if (listOfPosts[i].comments) {
      for (let j = 0; j < listOfPosts[i].comments.length; j += 1) {
        listOfPosts[i].comments[j].author == authorName ? commentsCnt += 1 : commentsCnt += 0;
      }
    }
  }
  return `Post:${postsCnt},comments:${commentsCnt}`;
};

const tickets = (peopl) => {
  const people = peopl;
  people.unshift(0);
  for (let i = 1; i < people.length; i += 1) {
    if (people.slice(0, i).reduce((prev, curr) => prev + curr, 0) / 25 < (people[i] - 25) / 25) {
      return 'NO';
    }
    people[i] -= people[i] - 25;
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
